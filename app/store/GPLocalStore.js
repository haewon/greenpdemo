Ext.define('GreenPDemo.store.GPLocalStore',{
    extend:'Ext.data.Store',
    config:{
        autoLoad:false,
        model:'GreenPDemo.model.GPLocalModel',
        storeId:'localStore'
    }
})