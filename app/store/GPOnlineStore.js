Ext.define('GreenPDemo.store.GPOnlineStore',{
    extend:'Ext.data.Store',
    requires:['Ext.data.proxy.LocalStorage'],
    config:{
        autoLoad:false,
        model:'GreenPDemo.model.GPOnlineModel',
        storeId:'onlineStore',
        proxy:{
            timeout:30000,
            type:'ajax',
            url: 'http://www1.toronto.ca/City_Of_Toronto/Information_&_Technology/Open_Data/Data_Sets/Assets/Files/greenPParking.json',
            //url: 'resources/json/greenPParking.json',
            reader:{
                type:'json',
                rootProperty:'carparks'
            }
        }        
    }
})