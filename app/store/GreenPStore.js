Ext.define('GreenPDemo.store.GreenPStore',{
    extend:'Ext.data.Store',
    required:[
        'GreenPDemo.model.CarParks'
    ],
    config:{
        autoLoad:false,
        model:'GreenPDemo.model.CarParks',
        storeId:'GreenPStore',
        proxy:{
            type:'ajax',
            url: 'resources/json/greenPParking.json',
            reader:{
                type:'json',
                rootProperty:'carparks'
            }
        }
        
    }
    
})