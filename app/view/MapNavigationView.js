Ext.define('GreenPDemo.view.MapNavigationView', {
    extend: 'Ext.navigation.View',
    xtype: 'mapnavigation',
    requires:['Ext.field.Search','Ext.MessageBox'],
    config: {
        autoDestroy: false,
        navigationBar:{
            items:{
                xtype:'searchfield',
                placeholder:'Address',
                align:'right'
            }
        },
        items: [
            {
                xtype:'parkingmap'
            }
        ]
    }
});
