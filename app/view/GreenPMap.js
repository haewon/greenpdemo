Ext.define('GreenPDemo.view.GreenPMap',{
    extend:'Ext.Map',
    xtype:'greenmap',
    requires:['GreenPDemo.view.plugin.Traffic'],
    config:{
         mapOptions:{
            zoom:(Ext.os.is.Phone) ? 13 : 15,
            mapTypeControl:true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                position: google.maps.ControlPosition.TOP_RIGHT
                },
            panControl: true,
            panControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.LEFT_TOP
                },
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.TOP_LEFT
                },
            center:new google.maps.LatLng(43.671845,-79.3927)
        },
        plugins:{
            xclass: 'GreenPDemo.view.plugin.Traffic',
            hidden: true
        },
    },
    
})