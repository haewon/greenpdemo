Ext.define('GreenPDemo.view.GreenPDetail',{
    extend:'Ext.Container',
    xtype:'detailview',
    config:{
        draggable:{
            id:'dragg',
            direction:'vertical',
            constraint:{
               min: { x: -Infinity, y: -Infinity },
               max: { x: Infinity, y: Infinity } 
            }
        },
        items:[{
            xtype:'button',
            itemId:'addressButton',
            iconCls:'plain'
        }]
    }
    
})