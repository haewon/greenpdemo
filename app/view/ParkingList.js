Ext.define('GreenPDemo.view.ParkingList', {
    extend: 'Ext.dataview.List',
    xtype: 'parkinglist',
    requires:['Ext.plugin.ListPaging'],
    config: {
        plugins:[{
            xclass:'Ext.plugin.ListPaging',
            autoPaging:false,
            loadMoreTest:'Loading...',
            noMoreRecordsTest:'No More Parking'
        }],
        emptyText:'emptyText',
        itemTpl: '<div><b>{xindex}.</b> {address}</div> <div>{rate}</div> <div>{lat}, {lng}: Distance: {distance}</div>',
        store:'localStore'
    }
});