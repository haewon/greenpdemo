Ext.define('GreenPDemo.view.ParkingDetail',{
    extend: 'Ext.form.Panel',
    xtype:'detailform',
    requires:['Ext.form.FieldSet','Ext.field.TextArea', 'Ext.MessageBox'],
    config: {
        title: 'Parking Details',
        itemId:'detailForm',
        scrollable:true,    
        items:[{
            xtype:'fieldset',
            itemsId:'parkingInfoFieldSet',
            title:'Parking Details',
            instructions:'Provide by Tokomode',
            items:[{
                xtype:'textfield',
                itemId:'addressField',
                label:'Address',
                name:'address',
                readonly:true,
                clearIcon:false
            },
            {
                xtype:'textfield',
                itemId:'rateField',
                label:'Half Hour Rate',
                name:'rate_half_hour',
                readonly:true,
                clearIcon:false
            },{
                xtype:'textfield',
                itemId:'typeeField',
                label:'Carpark Type',
                name:'carpark_type',
                readonly:true,
                clearIcon:false
            }
            ],
        },
        {
            xtype:'button',
            itemId:'StreetViewButton',
            text:'Street View'
        }
            
        ]
        
    }
    
})