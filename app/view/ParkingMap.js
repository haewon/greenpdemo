Ext.define('GreenPDemo.view.ParkingMap',{
    extend:'Ext.Map',
    xtype:'parkingmap',

    config: {
        itemId:'parkingMap',
        mapOptions:{
        zoom:(Ext.os.is.Phone) ? 12 : 14,
        mapTypeControl:true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.TOP_RIGHT
            },
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL,
            position: google.maps.ControlPosition.LEFT_TOP
            },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.TOP_LEFT
            }
        }
    }
    
})