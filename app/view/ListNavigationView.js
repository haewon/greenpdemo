Ext.define('GreenPDemo.view.ListNavigationView', {
    extend: 'Ext.navigation.View',
    xtype: 'listnavigation',
    config: {
        autoDestroy: false,
        fullscreen:true,
        items: [
            {
                xtype:'parkinglist'
            }
        ]
    }
});
