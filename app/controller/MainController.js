Ext.define('GreenPDemo.controller.MainController',{
    extend: 'Ext.app.Controller',
    requires:['Ext.device.Geolocation'],
    config:{
        refs:{
            map:'parkingmap',
            mapNaigation:'mapnavigation',
            list:'parkinglist',
            searchBar:'searchfield[placeholder="Address"]',
            listNavigation:'listnavigation',
            tabPanel:'tabpanel',
            streetViewButton:'button[itemId="StreetViewButton"]'
        },
        control:{
            list:{
                itemsingletap:'showParkingDetail',
                disclose:'showMap'
            },
            searchBar:{
                change:'searchAddress'
            },
            map:{
                maprender:'renderMap'
            },
            streetViewButton:{
                tap:'showStreetView'
            }
        }
    },
    showParkingDetail:function(list, index, target, item, e, eOpts){
        var detailView=Ext.widget('detailform');
        detailView.setRecord(item);
        this.selectItem=item;
        var delayedTask=Ext.create('Ext.util.DelayedTask', function(){
            list.deselect(index);
        });
        delayedTask.delay(500);
        this.getListNavigation().push(detailView);
    },
    showMap:function (list, record, target, index, e, eOpts){
        this.getTabPanel().animateActiveItem(0, {
        type:'slide',
        direction:'right'
        });
    },
    searchAddress:function(field){
        Ext.Msg.alert('Map Search',field.getValue());
    },
    renderMap:function(comp, map, eOpts){
        Ext.device.Geolocation.getCurrentPosition({
            scope: this.getMap(),
            success: function(position) {
                this.getMap().map.setMapCenter(position.coords);
                this.getMap().marker.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                this.position=position;
                console.log('success');
            },
            failure: function() {
                comp.setMasked(false);
                //Ext.device.Notification.show({
                //    title: 'Geolocation',
                //    message: 'Something went wrong.'
                //});
                console.log('faile');
            }
        });
        //var userMarker=new google.maps.Marker({
        //    position: new google.maps.LatLng(this.getMap().getMapCenter),
        //    map:map
        //});
        var parkingStore=Ext.getStore('GreenPStore');
        var markers=[];
        parkingStore.each(function(carpark, index, length){
            var position=new google.maps.LatLng(carpark.get('lat'),carpark.get('lng'));
            var parkingMarker=new google.maps.Marker({
                position:position,
                map:map,
                carpark:carpark,
                icon:'https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png'
                });
            
        parkingMarker.setClickable(true);
        markers.push(parkingMarker);
        google.maps.event.addListener(parkingMarker, 'mouseup', function() {
            var park = this.carpark;
            var content = '<span style="font-weight: bold">' + park.get('address') + '</span>';
            if (!comp.infoWindow) {
                comp.infoWindow = new google.maps.InfoWindow();
                }
                comp.infoWindow.setContent(content);
                comp.infoWindow.open(map, this);
                });
        });
        //var mcOptions = { gridSize: 50, maxZoom: 18 };
        //var markerCluster = new MarkerClusterer(map, markers, mcOptions);
    },
    showStreetView: function(button, e, eOpts) {
        var message='Lat: '+this.selectItem.get('streetview_lat')+' Lon: '+this.selectItem.get('streetview_long');
        Ext.Msg.alert('Street View',message);
    }
})