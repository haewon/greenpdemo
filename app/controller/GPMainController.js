Ext.define('GreenPDemo.controller.GPMainController',{
	extend:'Ext.app.Controller',
	requires:['Ext.device.Geolocation','Ext.Panel','Ext.MessageBox', 'GreenPDemo.view.plugin.Traffic'],
    config:{
        refs:{
            changeButton:'button[action=change]',
            searchButton:'button[action=search]',
            timerButton:'button[action=timer]',
            settingsButton:'button[action=setting]',
            toggleWatch:'gtoolbar #toolbar1 #watchToggle',
            searchbar:'gtoolbar #toolbar2 #searchBar',
            scancelButton:'button[action=searchCancel]',
            map:'greenmap',
            list:'parkinglist',
            detailView:'detailview',
            mainView:'mainview',
            gtoolbar:'gtoolbar',
            prevButton:'button[action=prev]',
            moreButton:'button[action=loadmore]',
            refreshButton:'button[action=refresh]',
            sortSegButton:'segmentedbutton[action=sorter]',
            filterSegButton:'segmentedbutton[action=filter]',
            nextButton:'button[action=next]'
        },
        control:{
            toggleWatch:{
                toggle:'turnWatch'
            },
            map:{
                maprender:'showMap'
            },
            changeButton:{
                tap:'changeView'
            },
            list:{
                itemsingletap:'selectParking'
            },
            searchbar:{
                action:'searchAddress'
            },
            
            settingsButton:{
                tap:'showTraffic'
            },
            searchButton:{
                tap:function(button, e, eOpts){
                    this.getGtoolbar().setActiveItem(1);
                }
            },
            scancelButton:{
                tap:function(button, e, eOpts){
                    this.getGtoolbar().setActiveItem(0);
                }
            },
            prevButton:{
                tap:function(button,e, eOpts){
                    if(this.getMap().currentMarker<1){
                        return;
                    }
                    else {
                        this.getMap().currentMarker--;
                    }
                    google.maps.event.trigger(this.getMap().markers[this.getMap().currentMarker], 'mouseup');
                }
            },
            nextButton:{
                tap:function(button,e, eOpts){
                    if(this.getMap().currentMarker==-1||this.getMap().currentMarker>this.getMap().markers.length-1){
                        return;
                    }
                    else {
                        this.getMap().currentMarker++;
                    }
                    google.maps.event.trigger(this.getMap().markers[this.getMap().currentMarker], 'mouseup');
                }
            },
            moreButton:{
                tap:function(button,e, eOpts){
                    if(this.currentPage<this.maxDistances.length-1){
                        this.currentPage++;
                        this.reloadNearBy();
                    }
                }
            },
            refreshButton:{
                tap:function(button,e, eOpts){
                    this.currentPage=0;
                    this.reloadNearBy();
                }
            },
            sortSegButton:{
                toggle:function(container, button, pressed){
                    if(pressed){
                        this.isPriceSort=button.getText()=='Price By';
                        this.sortStore();
                    }
                }
            },
            filterSegButton:{
                toggle:function(container, button, pressed){
                    if(pressed){
                        if(button.getText()=='Near By'){
                           this.reloadNearBy();
                        }
                        else if(button.getText()=='Favorites'){
                            this.reloadFav();
                        }
                        else{
                            this.reloadNote();
                        }
                    }
                }
            }
        }
    },
    isPriceSort:false,
    maxDistances:[],
    pageSize:10,
    currentPage:0,
    //store
    init:function(){
        //window.localStorage.clear();
        var localStore=Ext.getStore('localStore');
        localStore.load({
            callback:this.initLoad,
            scope:this
        });

    },

    initLoad:function(records, operation, success){
        if(records.length==0){
            var onlineStore=Ext.getStore('onlineStore');
            onlineStore.load({
                callback:function(records, operation, success){
                    var len=records.length;
                    var localStore=Ext.getStore('localStore');
                    for(var i=0;i<len;i++){
                        localStore.add(records[i]._data);
                        localStore.sync();
                    }
                    onlineStore.removeAll();
                    this.getUserLocation();
                },
                scope:this
            });
        }
        else{
            this.getUserLocation();
            console.log('localStorage found');
        }
    },

	setDistance:function(coords){
        var localStorage=Ext.getStore('localStore');
        localStorage.clearFilter();
        localStorage.each(function(carpark, index, length){
            var position=new google.maps.LatLng(carpark.get('lat'),carpark.get('lng'));
            carpark.set('distance',google.maps.geometry.spherical.computeDistanceBetween(position, coords));
			localStorage.sync();
        });
        localStorage.sort('distance','ASC');
        var len=localStorage.data.items.length;
        this.maxDistances=[];
        this.currentPage=0;
        for(var i=this.pageSize;i<len;i+=this.pageSize){
            var distance=localStorage.data.items[i].get('distance');
            this.maxDistances.push(distance);
        }
        if((len-1)%this.pageSize!=0){
            var distance=localStorage.data.items[len-1].get('distance');
            this.maxDistances.push(distance);
        }
        this.reloadNearBy();
    },

    reloadNearBy:function(){
        var localStorage=Ext.getStore('localStore');
        localStorage.clearFilter();
        localStorage.filterBy(function(record){
            if(record.get('distance')<this.maxDistances[this.currentPage]){
                return true;
            }
            else{
                return false;
            }
        },this);
        this.sortStore();
    },
    reloadFav:function(){
        var localStorage=Ext.getStore('localStore');
        localStorage.clearFilter();
        localStorage.filterBy(function(record){
            return record.get('favorite');
        });
        this.sortStore(); 
    },
    reloadNote:function(){
        var localStorage=Ext.getStore('localStore');
        localStorage.clearFilter();
        // localStorage.filterBy(function(record){
        //     return record.get('note');
        // });
        this.sortStore();
    },
    sortStore:function(){
        var localStorage=Ext.getStore('localStore');

        if(this.isPriceSort){
            localStorage.sort('rate','ASC');
        }
        else{
            localStorage.sort('distance','ASC');
        }
        this.showMarkers();
    },
    // touch
    launch: function() {
        // var drag=this.getDetailView().draggableBehavior.draggable;
        // drag.on({
        //     dragstart:{
        //         fn:this.dragStart,
        //         scope:this
        //     },
        //     drag:{
        //        fn:this.onDrag,
        //        scope:this
        //    }
        // })
        // this.getDetailView().element.on(['drag'],'onTouchEvent',this);
    },
    dragStart:function(draggable, e, offsetX, offsetY, eOpts){
        //debugger;
        this.mapY=this.getMap().getHeight();
        this.detailY=this.getDetailView().getHeight();
    },
    dragEnd:function(draggable, e, offsetX, offsetY, eOpts){
        this.getMap().setHeight(this.mapY+offsetY);
        this.getDetailView.setHeight(this.detailY-offsetY);
    },
    onDrag:function(draggable, evt, offsetX, offsetY, eOpts ) {
        //debugger;
        this.getMap().setHeight(this.mapY+offsetY);
        this.getDetailView.setHeight(this.detailY-offsetY);
    },
    onTouchEvent: function(e, target, options, eventController) {
        console.log(e);
    },

    //map
    turnWatch:function(toggles, button, isPressed, eOpts){
        Ext.device.Geolocation.clearWatch();
        this.watchPosition(isPressed);
    },
	watchPosition:function(isWatch){
        var parkingmap=this.getMap();
        Ext.device.Geolocation.watchPosition({
            frequency:5000,
            allowHighAccuracy:true,
            callback:function(position){
                if(isWatch){
                    parkingmap.setMapCenter(position.coords);
                }
                if(parkingmap.userMarker==undefined||parkingmap.userMarker==null){
                    parkingmap.userMarker=new google.maps.Marker({
                        position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                        map:parkingmap.getMap()
                    });
                    parkingmap.setMapCenter(position.coords);
                }
                parkingmap.user=new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                parkingmap.userMarker.setPosition(parkingmap.user);
                //parkingmap.coord=position.coords;
                // var marker=parkingmap.userMarker;
                // google.maps.event.addListener(marker, 'mouseup', function(){
                //     parkingmap.setMapCenter(this.coord);
                // });
            },
            failure:function(){
                console.log('Watch Fail')
            }
        });
    },

    getUserLocation:function(){
    	 Ext.device.Geolocation.getCurrentPosition({
            success:function(position){
                this.getMap().setMapCenter(position.coords);
                this.setDistance(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            },
            failure:function(){
                console.log('Get Current Position Fail');
            },
            scope:this
        });
    },

    showMap:function(component, map, eOpts){
    	component.isOpenInfo=false;
        this.watchPosition(false);
        component.markers=[];
        component.currentMarker=-1;
        component.directionsDisplay=new google.maps.DirectionsRenderer();
        google.maps.event.addListener(map,'click', function(){
            if(component.isOpenInfo){
                //component.infoWindow.close();
                component.infoBox.close();
                component.isOpenInfo=false;
                component.currentMarker=-1;
            }
            component.directionsDisplay.setMap(null);
        });
        
    },

    showMarkers:function(){
    	var map=this.getMap();
        if(map.isOpenInfo){
            map.infoBox.close();
            map.isOpenInfo=false;
            map.currentMarker=-1;
        }
    	var len=map.markers.length;
    	for(var i=0;i<len;i++){
    		map.markers[i].setMap(null);
    	}
    	var parkingStore=Ext.getStore('localStore');
        map.markers=[];
        parkingStore.each(function(carpark, index, length){
            var position=new google.maps.LatLng(carpark.get('lat'),carpark.get('lng'));
            var parkingMarker=new google.maps.Marker({
                position:position,
                map:map.getMap(),
                carpark:carpark,
                icon:'resources/icons/parking_lot_maps.png'
            });
            
            parkingMarker.setClickable(true);
            map.markers.push(parkingMarker);
            google.maps.event.addListener(parkingMarker, 'mouseup', function() {
                map.isOpenInfo=true;
                map.currentMarker=map.markers.indexOf(parkingMarker);
                var park = parkingMarker.carpark;
                // var content = '<span style="font-weight: bold">' + park.get('address') + '</span>';
                // if (map.infoWindow==undefined||map.infoWindow==null) {
                //     map.infoWindow = new google.maps.InfoWindow();
                //     google.maps.event.addListener(map.infoWindow,'closeclick', function(){
                //         map.isOpenInfo=false;
                //     });    
                // }
                // map.infoWindow.setContent(content);
                // map.infoWindow.open(map.getMap(), this);
                // var position=new google.maps.LatLng(park.get('lat'),park.get('lng'));
                // map.setMapCenter(position);
                var position=new google.maps.LatLng(park.get('lat'),park.get('lng'));

                if(map.user){
                    console.log(map.currentMarker);
                    var request={
                        origin:map.user,
                        destination:position,
                        travelMode: google.maps.TravelMode.DRIVING
                    }
                    var di=new google.maps.DirectionsService();
                    di.route(request,function(result, status){
                        if (status == google.maps.DirectionsStatus.OK) {
                            map.directionsDisplay.setMap(map.getMap());
                            map.directionsDisplay.setDirections(result);
                        }
                    });
                }
                var boxText = document.createElement("div");
		        boxText.id =park.get('id');
				boxText.class = 'callout';
		        boxText.style.cssText = "border: 0px solid black; margin-top: 0px; padding: 5px; color: white; font-size: 11px";
		        boxText.innerHTML = '[' + park.get('xindex') + '] ' +park.get('address') + '<br />' + park.get('distance');
	
		        var myOptions = {
		            content: boxText
		            ,disableAutoPan: false
		            ,maxWidth: 0
		            ,pixelOffset: new google.maps.Size(-107, -80)
		            ,zIndex: null
		            ,boxStyle: {
		                background: "url('resources/images/bg-callout.png') no-repeat",
		                opacity: 0.85,
		                width: "195px",
						height: "50px"
		            }
	                ,closeBoxMargin: "0px 0px 2px 2px"
	                ,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
	                ,infoBoxClearance: new google.maps.Size(1, 1)
	                ,isHidden: false
	                ,pane: "floatPane"
	                ,enableEventPropagation: false
	            };
	            if (map.infoBox==undefined||map.infoBox==null) {
					map.infoBox = new InfoBox();
						
					google.maps.event.addListener(map.infoBox, 'closeclick', function() {
					console.log('Info box will be closed.');
					if (map.isOpenInfo) {
						map.currentMarker = -1;
						map.isOpenInfo = !map.isOpenInfo;
						}
					});
				}	
				map.infoBox.setOptions(myOptions);
				map.infoBox.open(parkingMarker.map, this);
                map.setMapCenter(position);
                //map.markerPosition=position;
            });
        });
    },
    
    changeView:function(button, e, eOpts){
        if(button.getIconCls()=='organize'){
            this.getMainView().setActiveItem(1);
            button.setIconCls('maps');
        }
        else{
            this.getMainView().setActiveItem(0);
            button.setIconCls('organize');
        }
    },

    selectParking:function(list, index, target, item, e, eOpts){
        list.deselect(index);
        //this.getChangeButton().panel.hide('fadeOut');
        this.getMainView().setActiveItem(0);
        this.getChangeButton().setIconCls('organize');
        google.maps.event.trigger(this.getMap().markers[index],'mouseup');
    },

    searchAddress:function(field,e,eOpts){
        var address=field.getValue()+',Toronto,ON,Canada';
        var self=this;
        var geocoder=new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function(results, status){
            if(status=='OK'){
            	self.setDistance(results[0].geometry.location);
                self.getMap().setMapCenter(results[0].geometry.location);
                
            }
            else{
                Ext.Msg.alert('Search error','Map Search Error:'+status);        
            }
        
        });
    },    
    showTraffic:function(button, e, eOpts){
        var traffic=this.getMap().getPlugins()[0];
        traffic.setHidden(!traffic.getHidden());
    }
})