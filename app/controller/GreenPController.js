Ext.define('GreenPDemo.controller.GreenPController',{
    extend:'Ext.app.Controller',
    requires:['Ext.device.Geolocation','Ext.Panel','Ext.MessageBox', 'GreenPDemo.view.plugin.Traffic'],
    config:{
        refs:{
            //toggleWatch:'maintoolbar #watchToggle',
            
            changeButton:'button[action=change]',
            //nearButton:'button[action=near]',
            hideButton:'button[action=hide]',
            //searchbar:'maintoolbar #searchBar',
            searchButton:'button[action=search]',
            timerButton:'button[action=timer]',
            nearButton:'button[action=setting]',
            toggleWatch:'gtoolbar #toolbar1 #watchToggle',
            searchbar:'gtoolbar #toolbar2 #searchBar',
            scancelButton:'button[action=searchCancel]',
            map:'greenmap',
            list:'parkinglist',
            detailView:'detailview',
            detailButton:'detailview #addressButton',   
            mainView:'mainview',
            gtoolbar:'gtoolbar'
        },
        control:{
            toggleWatch:{
                toggle:'turnWatch'
            },
            map:{
                maprender:'showMap'
            },
            changeButton:{
                tap:'changeView'
            },
            list:{
                itemsingletap:'selectParking'
            },
            searchbar:{
                action:'searchAddress'
            },
            detailButton:{
                tap:'showMore'
            },
            hideButton:{
                tap:'showDetailFull'
            },
            nearButton:{
                tap:'selectNearParking'
            },
            searchButton:{
                tap:function(button, e, eOpts){
                    this.getGtoolbar().setActiveItem(1);
                }
            },
            scancelButton:{
                tap:function(button, e, eOpts){
                    this.getGtoolbar().setActiveItem(0);
                }
            },
        }
    },
    turnWatch:function(toggles, button, isPressed, eOpts){
        Ext.device.Geolocation.clearWatch();
        this.watchPosition(isPressed);
    },
    setDistance:function(coords){
        var parkingStore=Ext.getStore('localStore');
        parkingStore.each(function(carpark, index, length){
            var position=new google.maps.LatLng(carpark.get('lat'),carpark.get('lng'));
            carpark.set('distance',google.maps.geometry.spherical.computeDistanceBetween(position, coords));
        });
        parkingStore.sync();
    },
    showMap:function(component, map, eOpts){
        this.watchPosition(false);
        var self=this;
        Ext.device.Geolocation.getCurrentPosition({
            success:function(position){
                component.setMapCenter(position.coords);
                self.setDistance(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            },
            failure:function(){
                console.log('Get Current Position Fail');
            }
        });
        var parkingStore=Ext.getStore('localStore');
        component.markers=[];
        parkingStore.each(function(carpark, index, length){
            var position=new google.maps.LatLng(carpark.get('lat'),carpark.get('lng'));
            var parkingMarker=new google.maps.Marker({
                position:position,
                map:map,
                carpark:carpark,
                icon:'https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png'
            });
            
            parkingMarker.setClickable(true);
            component.markers.push(parkingMarker);
            google.maps.event.addListener(parkingMarker, 'mouseup', function() {
                component.isOpenInfo=true;
                component.oldCenter=component.getMap().getCenter();
                var park = this.carpark;
                var content = '<span style="font-weight: bold">' + park.get('address') + '</span>';
                if (!component.infoWindow) {
                    component.infoWindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(component.infoWindow,'closeclick', function(){
                        if(component.isOpenInfo){
                            component.isOpenInfo=false;
                            self.getDetailView().setFlex(0);
                            self.getDetailView().setHidden(true);
                            self.getHideButton().setDisabled(true);
                            component.setMapCenter(component.oldCenter);
                        }   
                    });    
                }
                self.getDetailView().setFlex(1);
                self.getDetailView().setHidden(false);
                self.getDetailButton().setText(park.get('address'));
                self.getHideButton().setDisabled(false);
                component.infoWindow.setContent(content);
                component.infoWindow.open(map, this);
                var position=new google.maps.LatLng(park.get('lat'),park.get('lng'));
                component.setMapCenter(position);
                component.markerPosition=position;
            });
        });
        google.maps.event.addListener(map,'click', function(){
            if(component.isOpenInfo){
                component.infoWindow.close();
                component.isOpenInfo=false;
                self.getDetailView().setFlex(0);
                self.getDetailView().setHidden(true);
                self.getHideButton().setDisabled(true);
                component.setMapCenter(component.oldCenter);
            }
        });
        
    },
    
    watchPosition:function(isWatch){
        var parkingmap=this.getMap();
        Ext.device.Geolocation.watchPosition({
            frequency:5000,
            allowHighAccuracy:true,
            callback:function(position){
                if(isWatch){
                    parkingmap.setMapCenter(position.coords);
                }
                if(!parkingmap.userMarker){
                    parkingmap.userMarker=new google.maps.Marker({
                        position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                        map:parkingmap.getMap()
                    });
                }
                parkingmap.userMarker.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                parkingmap.coord=position.coords;
                var marker=parkingmap.userMarker;
                google.maps.event.addListener(marker, 'mouseup', function(){
                    parkingmap.setMapCenter(this.coord);
                });
            },
            failure:function(){
                console.log('Watch Fail')
            }
        });
    },
    showList:function(button, e, eOpts){
        if (!button.panel) {
            button.panel = Ext.widget('panel', {
                padding: 20,
                width: 280,
                height: 360,
                modal: true,
                hideOnMaskTap: true,
                hidden: true,
                hideAnimation: 'fadeOut',
                showAnimation: 'fadeIn',
                layout:{
                        type:'fit'
                    },
                items:[{
                    xtype:'parkinglist'
                    
                }]
            });
        }
        button.panel.showBy(button);
    },
    changeView:function(button, e, eOpts){
        if(button.getText()=='List'){
            this.getMainView().setActiveItem(1);
            button.setText('Map');
        }
        else{
            this.getMainView().setActiveItem(0);
            button.setText('List');
        }
    },
    selectParking:function(list, index, target, item, e, eOpts){
        list.deselect(index);
        //this.getChangeButton().panel.hide('fadeOut');
        this.getMainView().setActiveItem(0);
        this.getChangeButton().setText('List');
        google.maps.event.trigger(this.getMap().markers[index],'mouseup');
    },
    searchAddress:function(field,e,eOpts){
        var address=field.getValue()+',Toronto,ON,Canada';
        var map=this.getMap();
        var geocoder=new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function(results, status){
            if(status=='OK'){
                map.setMapCenter(results[0].geometry.location);
            }
            else{
                Ext.Msg.alert('Search error','Map Search Error:'+status);        
            }
        
        });
    },
    init:function(){
        var localStore=Ext.getStore('localStore');
        localStore.loadPage();
        if(localStore.getCount()==0){
            var onlineStore=Ext.getStore('onlineStore');
            onlineStore.on('refresh', function(store,records){
               store.each(function(record){
                    localStore.add(record);
               });
               localStore.sync();

            });
            onlineStore.load();
        }
        else{
            console.log('found');
        }
    },
    launch: function() {
        //var drag=this.getDetailView().draggableBehavior.draggable;
        //drag.on({
        //    dragstart:{
        //        fn:this.dragStart,
        //        scope:this
        //    },
        //    drag:{
        //        fn:this.onDrag,
        //        scope:this
        //    }
        //})
        //this.getDetailView().element.on(['drag'],'onTouchEvent',this);
    },
    dragStart:function(draggable, e, offsetX, offsetY, eOpts){
        //debugger;
        this.mapY=this.getMap().getHeight();
        this.detailY=this.getDetailView().getHeight();
    },
    dragEnd:function(draggable, e, offsetX, offsetY, eOpts){
        this.getMap().setHeight(this.mapY+offsetY);
        this.getDetailView.setHeight(this.detailY-offsetY);
    },
    onDrag:function(draggable, evt, offsetX, offsetY, eOpts ) {
        //debugger;
        this.getMap().setHeight(this.mapY+offsetY);
        this.getDetailView.setHeight(this.detailY-offsetY);
    },
    onTouchEvent: function(e, target, options, eventController) {
        console.log(e);
    },
    
    showDetailFull:function(button, e, eOpts){
        if(this.getMap().getHidden()){
            this.getDetailView().setFlex(1);
            this.getDetailView().setHidden(false);
            this.getMap().setHidden(false);
            button.setText('Full Detail');
        }
        else{
            this.getMap().setHidden(true);
            button.setText('Show Map');
        }
    },
    showMore:function(button, e, eOpts){
        
        if(this.getDetailView().getFlex()==1){
            this.getDetailView().setFlex(3);
        }
        else{
            this.getDetailView().setFlex(1);
        }
        
        this.getMap().setMapCenter(this.getMap().markerPosition);
        
    },
    
    selectNearParking:function(button, e, eOpts){
        var traffic=this.getMap().getPlugins()[0];
        traffic.setHidden(!traffic.getHidden());
    }
})